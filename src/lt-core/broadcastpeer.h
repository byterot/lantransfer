#pragma once

#include "udp-discovery/udp_discovery_peer.hpp"
#include "udp-discovery/udp_discovery_ip_port.hpp"
#include "udp-discovery/udp_discovery_protocol.hpp"

#include "transferpeer.h"

#include <vector>
#include <string>
#include <cstdint>

class BroadcastPeer
{
private:
  const std::string name;
  const uint32_t port;


  udpdiscovery::PeerParameters peer_params;
  udpdiscovery::Peer peer;

public:
  BroadcastPeer(std::string n, uint32_t p, bool discoverable);
  ~BroadcastPeer();

  bool start();

  std::vector<TransferPeer> get_peers() const;
};


