#pragma once

#include <string>

struct TransferPeer
{
  std::string name;
  std::string address;
  int port;
  long last_updated;
};
