#include "filereceiverserver.h"
#include "netconstants.h"
#include "routines.h"
#include "socket.h"

#include <condition_variable>
#include <mutex>

FileReceiverServer::FileReceiverServer(int p) : port(p)
{

}

FileReceiverServer::~FileReceiverServer()
{
  if(running)
    stop();
}

void FileReceiverServer::notify_receiver_done(FileReceiver const& sender)
{
  file_received = true;
  for(auto it = receivers.begin(); it != receivers.end(); ++it) 
  {
    auto const& receiver = **it;
    if(receiver == sender)
    {
      receivers.erase(it);
      break;
    }
  }
  cv.notify_one();
}

void FileReceiverServer::await_file_received()
{
  std::unique_lock<std::mutex> lk(mutex);
  cv.wait(lk, [this] 
  { 
    return file_received; 
  });
}

void FileReceiverServer::start()
{
  server_thread = std::thread(&FileReceiverServer::run, this);
}

void FileReceiverServer::stop()
{
  running = false;

#if (WINDOWS)
  shutdown(socket_handle, 2);
  closesocket(socket_handle);
#else
  shutdown(socket_handle, SHUT_RDWR);
  close(socket_handle);
#endif

  if(server_thread.joinable())
    server_thread.join();
}

void FileReceiverServer::accept_client(SOCKET_TYPE socket)
{
  auto new_receiver = std::make_unique<FileReceiver>(*this, socket, preset_filename);
  receivers.push_back(std::move(new_receiver)); 
}

void FileReceiverServer::reject_client(SOCKET_TYPE socket)
{
#if (WINDOWS)
  shutdown(socket, 2);
  closesocket(socket);
#else
  shutdown(socket, SHUT_RDWR);
  close(socket);
#endif
}

#if (!WINDOWS)
void FileReceiverServer::run()
{
  // get socket handle
  socket_handle = socket(AF_INET, SOCK_STREAM, 0);

  if (socket_handle == 0)
  {
    Log::write("[FileReceive] Cannot create socket");
    return;
  }

  // attach server to port

  int32_t opt = 1;
  if (setsockopt(socket_handle, SOL_SOCKET, SO_REUSEADDR, &opt,
                 sizeof(opt)))
  {
    Log::write("[FileReceive] Cannot set socket options: " + std::string(strerror(errno)));
    return;
  }

  if (setsockopt(socket_handle, SOL_SOCKET, SO_REUSEPORT, &opt,
                 sizeof(opt)))
  {
    Log::write("[FileReceive] Cannot set socket options: " + std::string(strerror(errno)));
    return;
  }

  struct sockaddr_in addr;
  int32_t            addrlen = sizeof(addr);

  addr.sin_family      = AF_INET;
  addr.sin_addr.s_addr = INADDR_ANY;
  addr.sin_port        = htons(port);

  if (bind(socket_handle, (struct sockaddr *)&addr, sizeof(addr)) < 0)
  {
    Log::write("[FileReceive] Cannot bind socket");
    return;
  }

  if (listen(socket_handle, 3) < 0)
  {
    Log::write("[FileReceive] Cannot mark socket as accepting socket");
    return;
  }
#else

void FileReceiverServer::run()
{
  if (!NetcodeConstants::WSAInitialized)
  {
    WSADATA wsaData;
    int res = WSAStartup(MAKEWORD(2, 2), &wsaData);
    if (res != 0)
    {
      Log::write("[FileReceive] failed to initialize WSA: " + res);
    }
  }
  // get socket handle
  socket_handle = socket(AF_INET, SOCK_STREAM, 0);

  if (socket_handle == 0)
  {
    Log::write("[FileReceive] Cannot create socket");
    return;
  }

  // attach server to port
  const char opt = 1;
  if (setsockopt(socket_handle, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt)))
  {
    Log::write("[FileReceive] Cannot set socket options");
    return;
  }

  struct sockaddr_in addr;
  int32_t addrlen = sizeof(addr);

  addr.sin_family = AF_INET;
  addr.sin_addr.s_addr = INADDR_ANY;
  addr.sin_port = htons(port);

  if (bind(socket_handle, (struct sockaddr *)&addr, sizeof(addr)) ==
      SOCKET_ERROR)
  {
    Log::write("[FileReceive] Cannot bind socket");
    return;
  }

  if (listen(socket_handle, 3) == SOCKET_ERROR)
  {
    Log::write("[FileReceive] Cannot mark socket as accepting socket");
    return;
  }
#endif

  Log::write("[FileReceive] Server started listening");

  running = true;
  while (running)
  {
    int32_t new_socket =
    accept(socket_handle, (struct sockaddr *)&addr, (socklen_t *)&addrlen);

    if (new_socket < 0)
    {
      Log::write("[FileReceive] Stopping listener.");
      continue;
    }

    struct sockaddr_in* pV4Addr = (struct sockaddr_in*)&addr;
    struct in_addr ipAddr = pV4Addr->sin_addr;

    char str[INET_ADDRSTRLEN];
    inet_ntop( AF_INET, &ipAddr, str, INET_ADDRSTRLEN );
    std::string ipv4addr = std::string(str);

    Log::write("[FileReceive] Got connection from: " + ipv4addr);

    bool accepted = true;

    if(!preset_hostname.empty())
    {
      Log::write("[FileReceive] Verifying hostname...");
      auto peers = ltcore::routines::get_peers();
      for(auto peer: peers)
      {
        if(peer.address == ipv4addr)
        {
          Log::write("[FileReceive] Got connection from " + peer.name + "@" + peer.address);
          if(preset_hostname != peer.name)
          {
            accepted = false;

            Log::write("[FileReceive] But their hostname does not match the specified hostname to receive from.");
            Log::write("[FileReceive] Aborting connection");

            break;
          }
        }
      }
    }
   
    ltcore::socket::write_t<bool>(new_socket, accepted);

    if(accepted)
    {
      Log::write("[FileReceive] New client accepted");
      accept_client(new_socket);
    }
    else
    {
      Log::write("[FileReceive] New client rejected");
      reject_client(new_socket);
    }
  }
}
