#include "log.h"

#include <iostream>
#if !defined(WINXP)
#include <mutex>
#endif
#include <queue>

namespace Log
{
  namespace
  {
#if !defined(WINXP)
    std::mutex              log_lock;
#endif
    std::queue<std::string> messages;
  }

  bool log_enabled = false;
  bool progress_enabled = false;
  bool immediate = true;

  void write(const std::string &message)
  {
    if(!log_enabled)
      return;
#if !defined(WINXP)
    log_lock.lock();
#endif

    messages.push(message);

#if !defined(WINXP)
    log_lock.unlock();
#endif

    if(immediate)
      write_log();
  }

  void write_log()
  {
#if !defined(WINXP)
    log_lock.lock();
#endif

    while (!messages.empty())
    {
      if(log_enabled)
        std::cout << messages.front() << std::endl << std::flush;
      messages.pop();
    }

#if !defined(WINXP)
    log_lock.unlock();
#endif
  }
}
