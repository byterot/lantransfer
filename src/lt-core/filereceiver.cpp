#include "filereceiver.h"

#include <thread>
#include <algorithm>

#include "filereceiverserver.h"

#include "progress/impl/progressbar.h"

#include "socket.h"
#include "routines.h"

FileReceiver::FileReceiver(FileReceiverServer& p, SOCKET_TYPE s, std::string fn) : 
  parent(&p), socket_handle(s), filename(fn)
{
  //receive_thread = std::thread(&FileReceiver::run, this);
  run();
}

FileReceiver::~FileReceiver()
{
  //canceled = true;

  try
  {
#if (WINDOWS)
  shutdown(socket_handle, 2);
  closesocket(socket_handle);
#else
  shutdown(socket_handle, SHUT_RDWR);
  close(socket_handle);
#endif
  }
  catch(...)
  {
    // Suppress error
  }

  try
  {
    if(receive_thread.joinable())
      receive_thread.join();
  }
  catch(...)
  {
    // Suppress error
  }
}

void FileReceiver::run() 
{
  Log::write("[Server] accepted client.");

  if(filename.empty()) 
    filename = ltcore::socket::read_string(socket_handle);
  else
    (void)ltcore::socket::read_string(socket_handle);

  uint64_t size = ltcore::socket::read_t<uint64_t>(socket_handle);
  uint64_t total_read = 0;
  int last_percentage = 0;
  const size_t chunk_size = 1024 * 4;
  char* buffer = new char[chunk_size];

  FILE* fp = fopen(filename.c_str(), "wb");

  if(!fp)
  {
    Log::write("[FileReceive] could not write to output file: " + filename);
    parent->notify_receiver_done(*this);
  }

  Log::write("[FileReceive] begin to receive: " + filename + " (" + std::to_string(size) + ")");
  
  progressbar* progress = nullptr;
  if(Log::progress_enabled)
  {
    if(!Log::log_enabled)
      std::cout << ("[FileReceive] begin to receive: " + filename + " (" + std::to_string(size) + ")") << std::endl;
    std::cout << std::flush;

    progress = progressbar_new(filename.c_str(), 100);
  }

  while(total_read < size && !canceled)
  {
    try
    {
      size_t read = std::min((size_t)(size - total_read), (size_t)chunk_size);
      ltcore::socket::read_for(socket_handle, read, buffer); 
      total_read += read;
      
      int percentage = 10000 / ((size * 100) / total_read);
      if(Log::progress_enabled && percentage > last_percentage)
      {
        for(;last_percentage < percentage; ++last_percentage)
          progressbar_inc(progress);
      }

      fwrite(buffer, read, 1, fp);

      if(read < chunk_size)
        break;
    }
    catch(...)
    {
      break;
    }
  } 

  if(Log::progress_enabled)
    progressbar_finish(progress);

  received = true;

  fclose(fp);

  if(!canceled)
  {
    Log::write("[FileReceive] received: " + filename + " (" + std::to_string(size) + ")");
    parent->notify_receiver_done(*this);
  }
}
