#pragma once

#include "netcodecommon.h"
#include "filereceiver.h"

#include <condition_variable>
#include <mutex>

class FileReceiverServer
{
private:
  SOCKET_TYPE socket_handle;
  std::thread server_thread;
  std::condition_variable cv;
  std::mutex mutex;
  std::vector<std::unique_ptr<FileReceiver>> receivers;

  const std::string address = "127.0.0.1";
  int port;
  volatile bool running = false;

  void run();
  void accept_client(SOCKET_TYPE socket);
  void reject_client(SOCKET_TYPE socket);

  friend class FileReceiver;
  void notify_receiver_done(FileReceiver const& sender);
  volatile bool file_received = false;

public:
  std::string preset_filename = "";
  std::string preset_hostname = "";

  FileReceiverServer(int p);
  ~FileReceiverServer();

  void await_file_received();
  void start();
  void stop();

};
