#pragma once

#include "netcodecommon.h"

#include <condition_variable>
#include <mutex>

class FileSender
{
private:
  SOCKET_TYPE socket_handle;
  const std::string address;
  const int port;
  std::string filename;

  bool connected = false;
  bool file_sent = false;

  std::thread client_thread;
  std::condition_variable cv;
  std::mutex mutex;

  void run();
  void send_file();
  
public:
  FileSender(std::string addr, int p, std::string file);
  ~FileSender();

  void await_completion();
  void start();
  void stop();
};
