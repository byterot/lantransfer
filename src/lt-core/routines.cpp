#include "routines.h"

#include <chrono>
#include <thread>
#include <cstdint>

#include "log.h"
#include "filesender.h"
#include "hostname.h"
#include "lt-core/filereceiverserver.h"
#include "lt-core/filesender.h"

namespace ltcore
{
  namespace routines
  {
    bool receive_file(std::string filename, std::string hostname, std::string from_hostname, std::string secret) 
    { 
      if(hostname.empty())
        hostname = ltcore::hostname::get();

      BroadcastPeer broadcast(hostname, 50505, true);

      broadcast.start();

      Log::write("[Discovery] Made discoverable hostname: " + hostname);
      
      FileReceiverServer server(50506);
      server.preset_filename = filename;
      server.preset_hostname = from_hostname;

      server.start();

      Log::write("[FileReceive] Waiting to receive " + (filename == "" ? "a file" : filename) + " from " + (from_hostname == "" ? "anyone" : from_hostname));

      server.await_file_received();

      return true; 
    }

    bool send_file(std::string filename, std::string hostname, std::string to_hostname, std::string secret)
    {
      // start hostname discovery for receiver validation
      if(hostname.empty())
        hostname = ltcore::hostname::get();

      BroadcastPeer broadcast(hostname, 50505, true);

      broadcast.start();


      if(get_filesize(filename) == 0)
      {
        Log::write("[FileSend] Sending canceled because the file is empty or does not exist.");
        return false;
      }

      auto const peers = ltcore::routines::get_peers();
      if(peers.size() == 0)
      {
        Log::write("[FileSend] Sending canceled because no peers were found");
        return false;
      }

      TransferPeer const* target = &peers[0];

      if(to_hostname != "")
      {
        bool found = false;

        for(TransferPeer const& peer : peers)
        {
          if(peer.name == to_hostname)
          {
            found = true;
            target = &peer;
            break;
          }

          if(!found)
          {
            Log::write("[FileSend] Sending canceled because no peer with the hostname `" + to_hostname + "` was found");
            return false;
          }
        }
      }
      else
      {
        if(peers.size() > 1)
        {
          Log::write("[FileSend] Sending canceled because multiple peers were found");
          Log::write("[FileSend] Select a specific peer to send the file to");
          return false;
        }
      }

      FileSender sender(target->address, 50506, filename);

      sender.start();
      sender.await_completion();

      sender.stop();
      return true;
    }

    std::vector<TransferPeer> get_peers(int discovery_time)
    {
      Log::write("[Discovery] Looking for peers...");
      std::vector<TransferPeer> peers;

      auto hostname = ltcore::hostname::get();

      BroadcastPeer peer(hostname, 50505, false);

      if(!peer.start())
        return peers;

      while(discovery_time > 0 && peers.size() == 0) 
      {
        std::this_thread::sleep_for(std::chrono::seconds(1));
        peers = peer.get_peers();
        --discovery_time;
      }

      if(peers.size() == 0)
        Log::write("[Discovery] No peers found");
      else 
      {
        Log::write("[Discovery] " + std::to_string(peers.size()) + 
            (peers.size() == 1 ? " peer" : " peers") +
            " found");

        for(auto& p : peers)
        {
          Log::write("[Discovery] Peer: " + p.name + " at " + p.address);
        }
      }

      return peers;
    }

    size_t get_filesize(std::string filename)
    {
      FILE* fp = fopen(filename.c_str(), "rb");
      
      if(!fp)
        return 0;

      fseek(fp, 0, SEEK_END);

      size_t size = ftell(fp);

      fclose(fp);
      return size;
    }

  }
}
