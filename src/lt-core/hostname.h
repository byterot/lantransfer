#pragma once

#include "netcodecommon.h"

namespace ltcore
{
  namespace hostname
  {
    static std::string get() 
    {
#if (!WINDOWS)
      char name[128] = "";
      if(gethostname(name, sizeof(name)) == 0)
#else
      char name[MAX_COMPUTERNAME_LENGTH + 1] = "";
      if(GetComputerNameA(name, (LPDWORD)sizeof(name)))
#endif
        return std::string(name);

      return "error";
    }
  }
}
