#pragma once

#include <string>

namespace Log
{
  extern bool log_enabled;
  extern bool progress_enabled;
  extern void write(const std::string &message);
  extern void write_log();
}
