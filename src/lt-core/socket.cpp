#include "socket.h"

namespace ltcore
{
  namespace socket
  {
    void read_for(SOCKET_TYPE socket, uint32_t length, char* target)
    {
      int64_t r = 0;

      if(length == 0)
        return;

      while (r < length)
      {
#if (!WINDOWS)
        r += read(socket, target + r, (size_t)(length - r));
#else
        r += recv(socket, target + r, length - r, 0);
#endif
      }
    }

    void write(SOCKET_TYPE socket, uint32_t length, char* buffer)
    {
      uint32_t total_sent = 0;
      while(total_sent < length)
      {
        auto sent = send(socket, buffer + total_sent, length - total_sent, 0);
        total_sent += sent;
      }
    }

    void write_string(SOCKET_TYPE socket, std::string str)
    {
      auto length = str.length() + 1;
      char* buffer = new char[length];

      write_t(socket, (uint32_t)length);
      strcpy(buffer, str.c_str());
      write(socket, length, buffer);

      delete[] buffer;
    }

    std::string read_string(SOCKET_TYPE socket)
    {
      uint32_t length = read_t<uint32_t>(socket);
      char* buffer = new char[length];

      read_for(socket, length, buffer);
  
      std::string ret(buffer);
      delete[] buffer;

      return ret;
    }
  }
}
