#include "broadcastpeer.h"

#include <list>

#define APP_ID 9283748

BroadcastPeer::BroadcastPeer(std::string n, uint32_t p, bool discoverable) : name(n), port(p) 
{
  peer_params.set_port(port);
  peer_params.set_application_id(APP_ID);

  peer_params.set_can_discover(true);

  peer_params.set_can_be_discovered(discoverable);
}

BroadcastPeer::~BroadcastPeer()
{
  peer.Stop(true);
}

bool BroadcastPeer::start() 
{
  return peer.Start(peer_params, name);
}

std::vector<TransferPeer> BroadcastPeer::get_peers() const
{
  std::vector<TransferPeer> ret;
  std::list<udpdiscovery::DiscoveredPeer> new_discovered_peers = peer.ListDiscovered();
  
  auto size = new_discovered_peers.size();
  for(auto& p: new_discovered_peers)
  {
    ret.push_back(
        {
          p.user_data(),
          udpdiscovery::IpToString(p.ip_port().ip()),
          p.ip_port().port(),
          p.last_updated()
        });

  }

  return ret;
}
