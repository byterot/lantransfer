#pragma once
#include "lt-core/netcodecommon.h"

class FileReceiverServer;

class FileReceiver
{
  private:
    FileReceiverServer* parent;
    SOCKET_TYPE socket_handle;
    std::thread receive_thread;
    std::string filename;
    volatile bool received = false;
    volatile bool canceled = false;

    void run();
  public:
    FileReceiver(FileReceiverServer& p, SOCKET_TYPE s, std::string fn);
    ~FileReceiver();

    inline bool operator==(const FileReceiver& other) const
    {
      return socket_handle == other.socket_handle;
    }
};

