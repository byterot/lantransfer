#pragma once 

#include <string>
#include <vector> 
#include <cstdint>

#include "broadcastpeer.h"
#include "transferpeer.h"
#include "hostname.h"

namespace ltcore
{
  namespace routines
  {
    bool receive_file(std::string = "", std::string hostname = "", std::string from_hostname = "", std::string secret = "");
    bool send_file(std::string filename, std::string hostname = "", std::string to_hostname = "", std::string secret = "");

    std::vector<TransferPeer> get_peers(int discovery_time = 5);

    size_t get_filesize(std::string filename);
  }
}
