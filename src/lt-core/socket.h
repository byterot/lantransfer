#pragma once

#include "netcodecommon.h"
#include <cstdint>

namespace ltcore
{
  namespace socket
  {
    extern void read_for(SOCKET_TYPE socket, uint32_t length, char* target);
    extern void write(SOCKET_TYPE socket, uint32_t length, char* buffer);

    template<class T>
    inline void write_t(SOCKET_TYPE socket, T t)
    {
      write(socket, sizeof(T), (char*)&t);
    }

    extern void write_string(SOCKET_TYPE socket, std::string str);

    template<class T>
    inline T read_t(SOCKET_TYPE socket)
    {
      T res;
      char* buffer = new char[sizeof(T)];

      read_for(socket, sizeof(T), buffer);
      memcpy((void*)&res, (void const*)buffer, sizeof(T));

      delete[] buffer;
      return res;
    }

    extern std::string read_string(SOCKET_TYPE socket);
  }
}
