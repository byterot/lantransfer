#define LOG_ENABLED 0

#include <cstdint>
#include <functional>
#include <iostream>
#include <list>
#include <string>
#include <vector>
#include <chrono>
#include <string.h>
#include <mutex>
#include <thread>

#include "log.h"

#if defined(WIN32) || defined(_WIN32) ||                                       \
defined(__WIN32) && !defined(__CYGWIN__)
#define WINDOWS 1

#include <cstdio>
#include <winsock2.h>
#include <windows.h>
#include <winbase.h>

#include <ws2tcpip.h>

#else

#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>

#endif

#if (WINDOWS)
#define SOCKET_TYPE SOCKET
#else
#define SOCKET_TYPE int32_t
#endif
