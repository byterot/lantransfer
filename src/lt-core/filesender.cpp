#include "filesender.h"

#include <cstdio>

#include "progress/impl/progressbar.h"

#include "socket.h"
#include "routines.h"
#include "netconstants.h"

FileSender::FileSender(std::string addr, int p, std::string file) : address(addr), port(p), filename(file)
{
  
}

FileSender::~FileSender()
{
  client_thread.join();
  stop();
}

#if (!WINDOWS)
void FileSender::run()
{
  socket_handle = socket(AF_INET, SOCK_STREAM, 0);

  if (socket_handle < 0)
  {
    Log::write("[Client]: Could not create socket");
    return;
  }

  struct sockaddr_in serv_addr;
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port   = htons(port);

  if (inet_pton(AF_INET, address.c_str(), &serv_addr.sin_addr) <= 0)
  {
    Log::write("[Client]: Invalid / Unsupported address");
    return;
  }

  if (connect(socket_handle, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
  {
    Log::write("[Client]: Failed to connect");
    return;
  }

  Log::write("[Client]: Client connected to server at " + address);

  connected = true;
  send_file();
}
#else
void FileSender::run()
{
  if (!NetcodeConstants::WSAInitialized)
  {
    WSADATA wsaData;
    int     res = WSAStartup(MAKEWORD(2, 2), &wsaData);
    if (res != 0)
    {
      Log::write("[Client]: failed to initialize WSA: ");
    }
  }

  socket_handle = socket(AF_INET, SOCK_STREAM, 0);

  if (socket_handle == INVALID_SOCKET)
  {
    Log::write("[Client]: Could not create socket");
    return;
  }

  SOCKADDR_IN serv_addr;
  serv_addr.sin_family      = AF_INET;
  serv_addr.sin_port        = htons(port);
  serv_addr.sin_addr.s_addr = inet_addr(address.c_str());

  if (connect(socket_handle, (struct sockaddr *)&serv_addr,
              sizeof(serv_addr)) == SOCKET_ERROR)
  {
    Log::write("[Client]: Failed to connect");
    return;
  }

  Log::write("[Client]: Client connected to server at " + address);

  connected = true;
  send_file();
}
#endif

void FileSender::send_file()
{
  bool cansend = ltcore::socket::read_t<bool>(socket_handle);
  if(!cansend)
  {
    stop();
  }

  ltcore::socket::write_string(socket_handle, filename);

  size_t size = ltcore::routines::get_filesize(filename);

  // TODO: figure out why this sleep is necessary
  std::this_thread::sleep_for(std::chrono::seconds(1));
  
  ltcore::socket::write_t(socket_handle, (uint64_t)size);

  Log::write("[FileSend]: sending: " + filename + " (" + std::to_string(size) + ")");

  int last_percentage = 0;
  const size_t chunk_size = 1024 * 4;
  char* buffer = new char[chunk_size];

  FILE* fp = fopen(filename.c_str(), "rb");

  progressbar* progress = nullptr;
  if(Log::progress_enabled)
  {
    if(!Log::log_enabled)
      std::cout << ("[FileSend]: sending: " + filename + " (" + std::to_string(size) + ")") << std::endl;
    std::cout <<std::flush;

    progress = progressbar_new(filename.c_str(), 100);
  }

  while(ftell(fp) != size)
  {
    auto read = fread(buffer, 1, chunk_size, fp);
    ltcore::socket::write(socket_handle, read, buffer);

    int percentage = 10000 / ((size * 100) / ftell(fp));
    if(Log::progress_enabled && percentage > last_percentage)
    {
      for(;last_percentage < percentage; ++last_percentage)
        progressbar_inc(progress);
    }

    if(read < chunk_size)
      break;
  }

  if(Log::progress_enabled)
    progressbar_finish(progress);

  fclose(fp);

  Log::write("[FileSend]: sent: " + filename + " (" + std::to_string(size) + ")");

  stop();
}

void FileSender::await_completion()
{
  std::unique_lock<std::mutex> lk(mutex);
  cv.wait(lk, [this] { return file_sent; });
}

void FileSender::start()
{
  client_thread = std::thread(&FileSender::run, this);
}

void FileSender::stop()
{
#if (WINDOWS)
  shutdown(socket_handle, 2);
  closesocket(socket_handle);
#else
  shutdown(socket_handle, SHUT_RDWR);
  close(socket_handle);
#endif

  file_sent = true;
  connected = false;
  cv.notify_one();
}
