#include "lt-core/test.h"

#include "lt-core/broadcastpeer.h"
#include "lt-core/filereceiverserver.h"
#include "lt-core/filesender.h"
#include "lt-core/hostname.h"
#include "lt-core/routines.h"

#include <chrono>
#include <thread>

#include <iostream>

struct Arguments
{
  bool sender = false,
       receiver = false;
  bool verbose = false,
       progress = false;

  std::string hostname = "";
  std::string target = "";
  std::string filename = "";
};

static void usages()
{
  std::cout << "Usage : lt";
  std::cout << " [-s | -r] [-vp] [-h hostname] [-t target] \"filename\"" << std::endl;
}

int main(int argc, char* argv[])
{
  if(argc < 2)
  {
    usages();
    return 0;
  }
  std::vector<std::string> args;
  for(int i = 1; i < argc; ++i)
    args.push_back(std::string(argv[i]));

  if(argc == 2 && (args[0] == "--usage" || args[0] == "help" || args[0] == "-help"))
  {
    usages();
    std::cout << "Available options: " << std::endl;

    std::cout << " [-s | -r] Send or Receive the file" << std::endl;
    std::cout << " [-v] Verbose logging" << std::endl;
    std::cout << " [-p] Transfer progress indication" << std::endl;
    std::cout << " [-h] Custom hostname for this machine" <<std::endl;
    std::cout << " [-t] Target hostname to send to or receive from" <<std::endl;
    return 0;
  }

  Arguments arguments;

  for(int i = 0; i < args.size(); ++i)
  {
    if(args[i][0] == '-')
    {
      for(int j = 1; j < args[i].length(); ++j)
      {
        switch(args[i][j])
        {
          case 's':
            arguments.sender = true;
            break;
          case 'r':
            arguments.receiver = true;
            break;
          case 'v':
            arguments.verbose = true;
            break;
          case 'p':
            arguments.progress = true;
            break;
        }
      }

      continue;
    }

    if(args[i] == "-h")
    {
      if(i > args.size() - 1)
      {
        std::cout << "No value supplied for: " << args[i] << std::endl;
        return 0;
      }
      arguments.hostname = args[i+1];
      ++i;
    } 
    else if(args[i] == "-t")
    {
      if(i > args.size() - 1)
      {
        std::cout << "No value supplied for: " << args[i] << std::endl;
        return 0;
      }
      arguments.target = args[i+1];
      ++i;
    } 
    else if(i == args.size() - 1) 
    {
      arguments.filename = args[i];
    } 
    else 
    {
      std::cout << "Unsupported option supplied: " << args[i] << std::endl;
      return 0;
    }
  }

  if(arguments.sender && arguments.receiver)
  {
    std::cout << "Both -s and -r were provided, these are exclusive options." << std::endl;
    std::cout << "Please select either -s or -r." << std::endl;
    return 0;
  }

  if(arguments.filename.empty())
  {
    std::cout << "No filename specified." << std::endl;
    usages();
    return 0;
  }

  Log::log_enabled = arguments.verbose;
  Log::progress_enabled = arguments.progress;

  if(arguments.receiver)
  {
    ltcore::routines::receive_file(arguments.filename, arguments.hostname, arguments.target);
  } 
  else if(arguments.sender)
  {
    ltcore::routines::send_file(arguments.filename, arguments.hostname, arguments.target);
  } 
  else
  {
    std::cout << "Neither -s and -r were provided." << std::endl;
    std::cout << "Please select either -s or -r." << std::endl;
    return 0;
  }

  return 0;


  //ltcore::routines::receive_file("output-file.txt");

  ltcore::routines::send_file("../data/testfile.txt");

  auto hostname = ltcore::hostname::get();

  BroadcastPeer peer(hostname, 50505, true);

  if(!peer.start())
    std::cout << "Peer start failed" << std::endl;

  while(true) 
  {
    std::this_thread::sleep_for(std::chrono::seconds(1));
    auto peers = peer.get_peers();

    for(auto& p: peers)
    {
      std::cout << "Discovered peer " << p.name;
      std::cout << " at " << p.address << std::endl;
    }

    std::cout << "--------------------------------" << std::endl;
  }
}
