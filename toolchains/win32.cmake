# the name of the target operating system
SET(CMAKE_SYSTEM_NAME Windows)

add_definitions(-DWINDOWS)

# which compilers to use for C and C++
SET(CMAKE_C_COMPILER gcc)
SET(CMAKE_CXX_COMPILER g++)
SET(CMAKE_RC_COMPILER mingw32-windres)
