# the name of the target operating system
SET(CMAKE_SYSTEM_NAME Darwin)

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -L/usr/local/include -F/Library/Frameworks")
