#!/bin/sh -e

# This script hooks are used from my vim config
# If you're curious, here they are
# :nmap <F7> :exec '! ./debug.sh x'<CR>
# :nmap <F8> :exec '! ./debug.sh '.expand('%:p').':'.line('.')<CR>

PROJECT="lt"
DEBUGGER="cgdb"

./build.sh $PROJECT 1

if [ -z "$1" ]; then 
  cd bin
  $DEBUGGER $PROJECT
  cd ..
  exit 0
fi

if [ "$1" = "x" ]; then
  cd bin
  $DEBUGGER -ex 'r' --args $PROJECT -svp ../data/testfile.txt
  cd ..
  exit 0
fi

cd bin
$DEBUGGER -ex "b $1" -ex 'r' --args $PROJECT -svp ../data/testfile.txt
cd ..
