#!/bin/sh -e

# Lazy check if we're in the right directory 
if [ ! -d src ] || [ ! -f build.sh ] || [ ! -f run.sh ]; then 
	echo "-- Build cancelled, not in project directory"
	exit 1
fi

# Set up build and bin directories
mkdir -p build
mkdir -p bin

# Get the build target
if [ -z "$1" ]; then
  echo "-- No build target specified"
  exit 0
fi

BUILD_TARGET="$1"
OUTPUT_NAME="$1"
BUILD_TYPE=0

# Get the build type
if [ -z "$2" ]; then
  echo "-- No build type specified, assuming release build"
  echo "-- Build types are:"
  echo "-- 0 -> release build"
  echo "-- 1 -> debug build"
else
  BUILD_TYPE=$2
fi

# Get the platform
if echo "$(uname -a)" | grep -q "Linux"
then
	SYSTEM="linux"
elif echo "$(uname -a)" | grep -q "Darwin"
then
	SYSTEM="mac"
elif echo "$(uname -a)" | grep -q "MINGW32_NT-5" || echo "$(uname -a)" | grep -q "MINGW32_NT-6"
then
  SYSTEM="win-xp"
  OUTPUT_NAME="${OUTPUT_NAME}.exe"
else
	SYSTEM="windows"
  OUTPUT_NAME="${OUTPUT_NAME}.exe"
fi

cd build

# Set the toolchain dependent on platform
if [ "$SYSTEM" = "windows" ]; then
	echo "using windows toolchain"
	cmake .. -G "Unix Makefiles" -DCMAKE_TOOLCHAIN_FILE="toolchains/win32.cmake" -DBUILD_TYPE=$BUILD_TYPE
elif [ "$SYSTEM" = "win-xp" ]; then
	echo "using windows-xp toolchain"
	cmake .. -G "Unix Makefiles" -DCMAKE_TOOLCHAIN_FILE="toolchains/win32xp.cmake" -DBUILD_TYPE=$BUILD_TYPE
elif [ "$SYSTEM" = "mac" ]; then
	echo "using mac toolchain"
	cmake .. -G "Unix Makefiles" -DCMAKE_TOOLCHAIN_FILE="toolchains/mac.cmake" -DBUILD_TYPE=$BUILD_TYPE
else
	cmake .. -DBUILD_TYPE=$BUILD_TYPE
fi

# Get the amount of CPU threads for multithreaded compilation
if [ "$SYSTEM" = "mac" ]; then
  NUM_WORKERS=4 # most macs have at least 4 threads these days
else
  NUM_WORKERS=$(grep -P '^processor\t: [0-9]+$' /proc/cpuinfo | wc -l)
fi

echo "-- Starting make with $NUM_WORKERS workers"

if [ "$SYSTEM" = "windows" ] || [ "$SYSTEM" = "win-xp" ]; then
	mingw32-make -j$NUM_WORKERS
else
	make -j$NUM_WORKERS 
fi

cd ..

rm bin/* || true

echo $BUILD_TARGET
if [ "$SYSTEM" = "win-xp" ] || [ "$BUILD_TYPE" -eq 1 ]; then
  cp build/src/$BUILD_TARGET/$BUILD_TARGET bin/$OUTPUT_NAME
else
  upx -1 -o bin/$OUTPUT_NAME build/src/$BUILD_TARGET/$BUILD_TARGET || cp build/src/$BUILD_TARGET/$BUILD_TARGET bin/$OUTPUT_NAME && echo "-- UPX compression failed, copied output directly to bin"
fi
