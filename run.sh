#!/bin/sh -e

PROJECT="lt"

if [ ! -z "$1" ]; then
  PROJECT=$1
fi

./build.sh $PROJECT $2

cd bin

if echo "$(uname -a)" | grep -q "Linux"
then
  echo "-- Starting linux version of ${PROJECT}"
elif echo "$(uname -a)" | grep -q "Darwin"
then
  echo "-- Starting mac version of ${PROJECT}"
elif echo "$(uname -a)" | grep -q "MINGW32_NT-5" || echo "$(uname -a)" | grep -q "MINGW32_NT-6"
then
  echo "-- Starting windows version of ${PROJECT}"
  PROJECT="${PROJECT}.exe"
else
  echo "-- Starting windows version of ${PROJECT}"
  PROJECT="${PROJECT}.exe"
fi

./$PROJECT
